<?php

namespace App\Domain\Job\Service;

use App\Domain\Job\Repository\JobListRepository;
use App\Exception\ValidationException;

/**
 * Service.
 */
final class JobList
{
    private $repository;

    public function __construct(JobListRepository $repository)
    {
        $this->repository = $repository;
    }

    public function getList($offset, $search)
    {
        $users = $this->repository->getList($offset, $search);

        return $users;
    }
}