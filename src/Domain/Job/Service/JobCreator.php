<?php

namespace App\Domain\Job\Service;

use App\Domain\Job\Repository\JobCreatorRepository;
use App\Exception\ValidationException;

/**
 * Service.
 */
final class JobCreator
{
    private $repository;

    public function __construct(JobCreatorRepository $repository)
    {
        $this->repository = $repository;
    }

    public function create(array $data): int
    {
        $id = $this->repository->insert($data);
        
        return $id;
    }
}