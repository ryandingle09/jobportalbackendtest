<?php

namespace App\Domain\Job\Service;

use App\Domain\Job\Repository\DeleteRepository;
use App\Exception\ValidationException;

/**
 * Service.
 */
final class Delete
{
    private $repository;

    public function __construct(DeleteRepository $repository)
    {
        $this->repository = $repository;
    }

    public function delete($id)
    {
        return $this->repository->delete($id);
    }
}