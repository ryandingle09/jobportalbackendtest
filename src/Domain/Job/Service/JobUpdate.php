<?php

namespace App\Domain\Job\Service;

use App\Domain\Job\Repository\JobUpdateRepository;
use App\Exception\ValidationException;

/**
 * Service.
 */
final class JobUpdate
{
    private $repository;

    public function __construct(JobUpdateRepository $repository)
    {
        $this->repository = $repository;
    }

    public function update(array $data): int
    {
        $id = $this->repository->update($data);
        
        return $id;
    }
}