<?php

namespace App\Domain\Job\Service;

use App\Domain\Job\Repository\JobGetRepository;
use App\Exception\ValidationException;

/**
 * Service.
 */
final class JobGet
{
    private $repository;

    public function __construct(JobGetRepository $repository)
    {
        $this->repository = $repository;
    }

    public function get($id)
    {
        $users = $this->repository->get($id);

        return $users;
    }
}