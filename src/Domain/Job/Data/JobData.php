<?php

namespace App\Domain\Job\Data;

final class JobData
{
    public $id;
    public $title;
    public $description;
    public $employer_id;
    public $created_at;
    public $updated_at;
}