<?php

namespace App\Domain\Job\Repository;

use PDO;

/**
 * Repository.
 */
class JobCreatorRepository
{
    /**
     * @var PDO The database connection
     */
    private $connection;

    /**
     * Constructor.
     *
     * @param PDO $connection The database connection
     */
    public function __construct(PDO $connection)
    {
        $this->connection = $connection;
    }

    public function insert(array $data): int
    {
        $row = [
            'title' => $data['title'],
            'description' => $data['description'],
            'employer_id' => $data['employer_id'],
            'created_at' => date('Y-m-d m:i:s'),
            'updated_at' => date('Y-m-d m:i:s')
        ];

        $sql = "INSERT INTO jobs SET 
                title=:title, 
                description=:description, 
                employer_id=:employer_id, 
                created_at=:created_at, 
                updated_at=:updated_at";

        $this->connection->prepare($sql)->execute($row);

        return (int)$this->connection->lastInsertId();
    }
}