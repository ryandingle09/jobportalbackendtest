<?php

namespace App\Domain\Job\Repository;

use PDO;

/**
 * Repository.
 */
class JobListRepository
{
    /**
     * @var PDO The database connection
     */
    private $connection;

    /**
     * Constructor.
     *
     * @param PDO $connection The database connection
     */
    public function __construct(PDO $connection)
    {
        $this->connection = $connection;
    }

    public function getList($offset = 0, $search)
    {
        $sql = "SELECT * FROM jobs order by id DESC LIMIT 10 OFFSET ".$offset."";

        if($search)
            $sql = "SELECT * FROM jobs WHERE title like '%".$search."%' order by id DESC LIMIT 10 OFFSET ".$offset."";

        $res = $this->connection->prepare($sql);
        $res->execute();

        $result = $res->fetchAll();

        return $result;
    }
}