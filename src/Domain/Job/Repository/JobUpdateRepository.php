<?php

namespace App\Domain\Job\Repository;

use PDO;

/**
 * Repository.
 */
class JobUpdateRepository
{
    /**
     * @var PDO The database connection
     */
    private $connection;

    /**
     * Constructor.
     *
     * @param PDO $connection The database connection
     */
    public function __construct(PDO $connection)
    {
        $this->connection = $connection;
    }

    public function update(array $data): int
    {
        $row = [
            'title' => $data['title'],
            'description' => $data['description'],
            'employer_id' => $data['employer_id'],
            'updated_at' => date('Y-m-d m:i:s'),
            'id' => $data['id']
        ];

        $sql = "UPDATE jobs SET 
                title=:title, 
                description=:description, 
                employer_id=:employer_id, 
                updated_at=:updated_at WHERE id=:id";

        $this->connection->prepare($sql)->execute($row);

        return (int)$this->connection->lastInsertId();
    }
}