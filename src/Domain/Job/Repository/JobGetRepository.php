<?php

namespace App\Domain\Job\Repository;

use PDO;

/**
 * Repository.
 */
class JobGetRepository
{
    /**
     * @var PDO The database connection
     */
    private $connection;

    /**
     * Constructor.
     *
     * @param PDO $connection The database connection
     */
    public function __construct(PDO $connection)
    {
        $this->connection = $connection;
    }

    public function get($id)
    {
        $sql = "SELECT * FROM jobs WHERE id = ".$id."";

        $res = $this->connection->prepare($sql);
        $res->execute();

        $result = $res->fetch();

        return $result;
    }
}