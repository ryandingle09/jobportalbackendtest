<?php

namespace App\Domain\Job\Repository;

use PDO;

/**
 * Repository.
 */
class DeleteRepository
{
    /**
     * @var PDO The database connection
     */
    private $connection;

    /**
     * Constructor.
     *
     * @param PDO $connection The database connection
     */
    public function __construct(PDO $connection)
    {
        $this->connection = $connection;
    }

    public function delete($id)
    {
        $sql = "DELETE FROM jobs WHERE id = ".$id."";

        $res = $this->connection->prepare($sql);
        $a = $res->execute();
        return $a;
    }
}