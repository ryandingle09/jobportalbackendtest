<?php

namespace App\Domain\Applicant\Service;

use App\Domain\Applicant\Repository\ApplicantListRepository;
use App\Exception\ValidationException;

/**
 * Service.
 */
final class ApplicantList
{
    private $repository;

    public function __construct(ApplicantListRepository $repository)
    {
        $this->repository = $repository;
    }

    public function getList($offset, $job_id, $search)
    {
        $users = $this->repository->getList($offset, $job_id, $search);

        return $users;
    }
}