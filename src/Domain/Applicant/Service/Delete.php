<?php

namespace App\Domain\Applicant\Service;

use App\Domain\Applicant\Repository\DeleteRepository;
use App\Exception\ValidationException;

/**
 * Service.
 */
final class Delete
{
    private $repository;

    public function __construct(DeleteRepository $repository)
    {
        $this->repository = $repository;
    }

    public function delete($id)
    {
        $users = $this->repository->delete($id);

        return $users;
    }
}