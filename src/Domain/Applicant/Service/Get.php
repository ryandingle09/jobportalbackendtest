<?php

namespace App\Domain\Applicant\Service;

use App\Domain\Applicant\Repository\GetRepository;
use App\Exception\ValidationException;

/**
 * Service.
 */
final class Get
{
    private $repository;

    public function __construct(GetRepository $repository)
    {
        $this->repository = $repository;
    }

    public function get($id)
    {
        $users = $this->repository->get($id);

        return $users;
    }
}