<?php

namespace App\Domain\Applicant\Service;

use App\Domain\Applicant\Repository\ApplicantCreatorRepository;
use App\Exception\ValidationException;

/**
 * Service.
 */
final class ApplicantCreator
{
    private $repository;

    public function __construct(ApplicantCreatorRepository $repository)
    {
        $this->repository = $repository;
    }

    public function create(array $data)
    {
        $id = $this->repository->insert($data);
        return $id;
    }
}