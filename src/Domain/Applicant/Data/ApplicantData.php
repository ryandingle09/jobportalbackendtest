<?php

namespace App\Domain\Applicant\Data;

final class ApplicantData
{
    public $id;
    public $email;
    public $first_name;
    public $last_name;
    public $resume;
    public $job_id;
    public $created_at;
    public $updated_at;
}