<?php

namespace App\Domain\Applicant\Repository;

use PDO;

/**
 * Repository.
 */
class GetRepository
{
    /**
     * @var PDO The database connection
     */
    private $connection;

    /**
     * Constructor.
     *
     * @param PDO $connection The database connection
     */
    public function __construct(PDO $connection)
    {
        $this->connection = $connection;
    }

    public function get($id)
    {
        $sql = "SELECT * FROM applicants WHERE id = ".$id."";

        $res = $this->connection->prepare($sql);
        $res->execute();

        $result = $res->fetch();

        return $result;
    }
}