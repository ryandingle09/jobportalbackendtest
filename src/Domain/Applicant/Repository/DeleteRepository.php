<?php

namespace App\Domain\Applicant\Repository;

use PDO;

/**
 * Repository.
 */
class DeleteRepository
{
    /**
     * @var PDO The database connection
     */
    private $connection;

    /**
     * Constructor.
     *
     * @param PDO $connection The database connection
     */
    public function __construct(PDO $connection)
    {
        $this->connection = $connection;
    }

    public function delete($id)
    {
        // get old
        $sql2 = "SELECT resume FROM applicants WHERE id = ".$id."";
        $res2 = $this->connection->prepare($sql2);
        $res2->execute();

        $a2 = $res2->fetch();

        // delete
        $sql = "DELETE FROM applicants WHERE id = ".$id."";
        $res = $this->connection->prepare($sql);
        $a = $res->execute();

        return $a2['resume'];
    }
}