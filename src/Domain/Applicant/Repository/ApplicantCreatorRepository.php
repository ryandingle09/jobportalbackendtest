<?php

namespace App\Domain\Applicant\Repository;

use PDO;

/**
 * Repository.
 */
class ApplicantCreatorRepository
{
    /**
     * @var PDO The database connection
     */
    private $connection;

    /**
     * Constructor.
     *
     * @param PDO $connection The database connection
     */
    public function __construct(PDO $connection)
    {
        $this->connection = $connection;
    }

    public function insert(array $data): int
    {
        $row = [
            'full_name' => $data['full_name'],
            'email' => $data['email'],
            'job_id' => $data['job_id'],
            'resume' => $data['resume'],
            'contact' => $data['contact'],
            'created_at' => date('Y-m-d m:i:s'),
            'updated_at' => date('Y-m-d m:i:s')
        ];

        $sql = "INSERT INTO applicants SET 
                full_name=:full_name, 
                email=:email, 
                job_id=:job_id, 
                contact=:contact, 
                resume=:resume, 
                created_at=:created_at, 
                updated_at=:updated_at";

        $this->connection->prepare($sql)->execute($row);

        return (int)$this->connection->lastInsertId();
    }
}