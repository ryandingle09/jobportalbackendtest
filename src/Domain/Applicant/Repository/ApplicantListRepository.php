<?php

namespace App\Domain\Applicant\Repository;

use PDO;

/**
 * Repository.
 */
class ApplicantListRepository
{
    /**
     * @var PDO The database connection
     */
    private $connection;

    /**
     * Constructor.
     *
     * @param PDO $connection The database connection
     */
    public function __construct(PDO $connection)
    {
        $this->connection = $connection;
    }

    public function getList($offset = 0, $job_id = false, $search)
    {
        $sql = "SELECT * FROM applicants order by id DESC LIMIT 10 OFFSET ".$offset."";

        if($job_id)
            $sql = "SELECT * FROM applicants WHERE job_id=".$job_id." order by id DESC LIMIT 10 OFFSET ".$offset."";

        if($search)
            $sql = "SELECT * FROM applicants WHERE email LIKE '%".$search."%' order by id DESC LIMIT 10 OFFSET ".$offset."";

        $res = $this->connection->prepare($sql);
        $res->execute();

        $result = $res->fetchAll();

        return $result;
    }
}