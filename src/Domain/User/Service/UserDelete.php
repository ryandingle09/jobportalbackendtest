<?php

namespace App\Domain\User\Service;

use App\Domain\User\Repository\UserDeleteRepository;

/**
 * Service.
 */
final class UserDelete
{
    private $repository;

    public function __construct(UserDeleteRepository $repository)
    {
        $this->repository = $repository;
    }

    public function delete($id)
    {
        $user = $this->repository->delete($id);

        return $user;
    }
}