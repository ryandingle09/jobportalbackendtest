<?php

namespace App\Domain\User\Service;

use App\Domain\User\Repository\UpdateRepository;

/**
 * Service.
 */
final class UserUpdate
{
    private $repository;

    public function __construct(UpdateRepository $repository)
    {
        $this->repository = $repository;
    }

    

    public function checkExists($data)
    {
        $user = $this->repository->checkExists($data);

        return $user;
    }

    public function update($data)
    {
        $user = $this->repository->update($data);

        return $user;
    }
}