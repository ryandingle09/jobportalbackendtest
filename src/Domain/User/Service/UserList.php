<?php

namespace App\Domain\User\Service;

use App\Domain\User\Repository\UserListRepository;

/**
 * Service.
 */
final class UserList
{
    private $repository;

    public function __construct(UserListRepository $repository)
    {
        $this->repository = $repository;
    }

    public function getUsers($offset, $user_type, $search)
    {
        $users = $this->repository->getUsers($offset, $user_type, $search);

        return $users;
    }
}