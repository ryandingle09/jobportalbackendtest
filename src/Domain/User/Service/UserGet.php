<?php

namespace App\Domain\User\Service;

use App\Domain\User\Repository\UserGetRepository;

/**
 * Service.
 */
final class UserGet
{
    private $repository;

    public function __construct(UserGetRepository $repository)
    {
        $this->repository = $repository;
    }

    public function get($id)
    {
        $user = $this->repository->get($id);

        return $user;
    }
}