<?php

namespace App\Domain\User\Service;

use App\Domain\User\Repository\CheckRepository;

/**
 * Service.
 */
final class UserCheck
{
    private $repository;

    public function __construct(CheckRepository $repository)
    {
        $this->repository = $repository;
    }

    public function get($email, $password)
    {
        $user = $this->repository->get($email, $password);

        return $user;
    }
}