<?php

namespace App\Domain\User\Repository;

use PDO;

/**
 * Repository.
 */
class CheckRepository
{
    /**
     * @var PDO The database connection
     */
    private $connection;

    /**
     * Constructor.
     *
     * @param PDO $connection The database connection
     */
    public function __construct(PDO $connection)
    {
        $this->connection = $connection;
    }

    public function get($email, $password)
    {
        $sql = "SELECT email, password, user_type, id FROM users WHERE email = '$email'";
        $res = $this->connection->prepare($sql);
        $res->execute();

        $user = $res->fetch();

        if($user && password_verify($password, $user['password']))
        {
            $ar = [
                'email' => $user['email'], 
                'user_type' => $user['user_type'], 
                'id' => $user['id']
            ];

            return $ar;
        }
        else
        {
            return false;
        }
    }
}