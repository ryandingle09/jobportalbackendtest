<?php

namespace App\Domain\User\Repository;

use PDO;

/**
 * Repository.
 */
class UpdateRepository
{
    /**
     * @var PDO The database connection
     */
    private $connection;

    /**
     * Constructor.
     *
     * @param PDO $connection The database connection
     */
    public function __construct(PDO $connection)
    {
        $this->connection = $connection;
    }

    public function checkExists($user)
    {
        $s1     = "SELECT email from users where email = '".$user['email']."'";
        $check  = $this->connection->prepare($s1);
        $check->execute();

        $res    = $check->fetchAll();

        return count($res);
    }

    public function update($user): int
    {
        $pass = password_hash($user['password'], PASSWORD_BCRYPT);

        $row = [
            'email' => $user['email'],
            'first_name' => $user['first_name'],
            'last_name' => $user['last_name'],
            'user_type' => $user['user_type'],
            'password' => $pass,
            'updated_at' => date('Y-m-d m:i:s'),
            'id' => $user['id']
        ];

        $sql = "UPDATE users SET 
                email=:email, 
                first_name=:first_name, 
                last_name=:last_name, 
                user_type=:user_type,
                password=:password,
                updated_at=:updated_at WHERE id=:id";
        
        $update = $this->connection->prepare($sql)->execute($row);
        

        return $update;
    }
}