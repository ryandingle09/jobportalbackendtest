<?php

namespace App\Domain\User\Repository;

use PDO;

/**
 * Repository.
 */
class UserGetRepository 
{
    /**
     * @var PDO The database connection
     */
    private $connection;

    /**
     * Constructor.
     *
     * @param PDO $connection The database connection
     */
    public function __construct(PDO $connection)
    {
        $this->connection = $connection;
    }

    public function get($id)
    {
        $sql = "SELECT id, email, first_name, last_name, user_type, created_at, updated_at FROM users WHERE id = ".$id."";

        $res = $this->connection->prepare($sql);
        $res->execute();

        $result = $res->fetch();

        return $result;
    }
}