<?php

namespace App\Domain\User\Repository;

use PDO;

/**
 * Repository.
 */
class UserCreatorRepository
{
    /**
     * @var PDO The database connection
     */
    private $connection;

    /**
     * Constructor.
     *
     * @param PDO $connection The database connection
     */
    public function __construct(PDO $connection)
    {
        $this->connection = $connection;
    }

    /**
     * Insert user row.
     *
     * @param array $user The user
     *
     * @return int The new ID
     */
    public function insertUser(array $user): int
    {
        $pass = password_hash($user['password'], PASSWORD_BCRYPT);

        $row = [
            'email' => $user['email'],
            'first_name' => $user['first_name'],
            'last_name' => $user['last_name'],
            'user_type' => $user['user_type'],
            'password' => $pass,
            'created_at' => date('Y-m-d m:i:s'),
            'updated_at' => date('Y-m-d m:i:s')
        ];

        $sql = "INSERT INTO users SET 
                email=:email, 
                first_name=:first_name, 
                last_name=:last_name, 
                user_type=:user_type,
                password=:password,
                created_at=:created_at,
                updated_at=:updated_at";

        $this->connection->prepare($sql)->execute($row);

        return (int)$this->connection->lastInsertId();
    }

    public function checkEmail($email)
    {
        $sql = "SELECT email FROM users WHERE email=:email";

        $res = $this->connection->prepare($sql);
        $res->execute(['email' => $email]);

        $result = $res->fetchAll();

        return count($result);
    }
}