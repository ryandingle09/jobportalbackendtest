<?php

namespace App\Domain\User\Repository;

use PDO;

/**
 * Repository.
 */
class UserListRepository
{
    /**
     * @var PDO The database connection
     */
    private $connection;

    /**
     * Constructor.
     *
     * @param PDO $connection The database connection
     */
    public function __construct(PDO $connection)
    {
        $this->connection = $connection;
    }

    public function getUsers($offset = 0, $user_type = 1, $search)
    {
        $sql = "SELECT id, email, first_name, last_name, user_type, created_at, updated_at FROM users WHERE user_type = ".$user_type." order by id DESC LIMIT 10 OFFSET ".$offset."";
        
        if($search)
            $sql = "SELECT id, email, first_name, last_name, user_type, created_at, updated_at FROM users WHERE user_type = ".$user_type." and email like '%".$search."%' order by id DESC LIMIT 10 OFFSET ".$offset."";

        $res = $this->connection->prepare($sql);
        $res->execute();

        $result = $res->fetchAll();

        return $result;
    }
}