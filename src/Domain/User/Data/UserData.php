<?php

namespace App\Domain\User\Data;

final class UserData
{
    public $id;
    public $email;
    public $first_name;
    public $last_name;
    public $user_type;
    public $password;
    public $created_at;
    public $updated_at;
}