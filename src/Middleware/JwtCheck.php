<?php

namespace App\Middleware;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Slim\Routing\RouteContext;
use \Firebase\JWT\JWT;

final class JwtCheck implements MiddlewareInterface
{
    /**
     * Invoke middleware.
     *
     * @param ServerRequestInterface $request The request
     * @param RequestHandlerInterface $handler The handler
     *
     * @return ResponseInterface The response
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $response = $handler->handle($request);

        $jwt = 'test';
        $secret_key = 'thisissamplesecretnigga';

        $decoded = JWT::decode($jwt, $secret_key, array('HS256'));

        //Before Middleware
        $response->getBody()->write("Before Middleware");
        //Response Middleware
        //$response = $next($request, $response);
        //After Middleware
        $response->getBody()->write("After Middleware");

        return $response;
    }
}