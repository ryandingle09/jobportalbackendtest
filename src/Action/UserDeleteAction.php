<?php

namespace App\Action;

use App\Domain\User\Service\UserDelete;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class UserDeleteAction
{
    private $Delete;

    public function __construct(UserDelete $UserDelete)
    {
        $this->UserDelete = $UserDelete;
    }

    public function __invoke(
        ServerRequestInterface $request, 
        ResponseInterface $response
    ): ResponseInterface {
        $id             = $request->getQueryParams('id')['id'] ? $request->getQueryParams('id')['id'] : false;
        $status_code    = 201;
        $status         = 'success';
        $message        = 'Successfully Deleted!';
        $result         = [];

        $data = $this->UserDelete->delete($id);

        if(!$data)
        {
            $data       = $id;
            $status     = 'error';
            $message    = 'Unexpected Error Encountered! Contact system administrator!';
        }

        $result = [
            'status'    => $status,
            'message'   => $message,
            'data'      => $id,
        ];

        $response->getBody()->write((string)json_encode($result));
        return $response->withHeader('Content-Type', 'application/json')->withStatus($status_code);
    }
}