<?php

namespace App\Action;

use App\Domain\User\Service\UserList;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class UserListAction
{
    private $userList;

    public function __construct(UserList $userList)
    {
        $this->userList = $userList;
    }

    public function __invoke(
        ServerRequestInterface $request, 
        ResponseInterface $response
    ): ResponseInterface {
        $page       = $request->getQueryParams('page')['page'] ? $request->getQueryParams('page')['page'].'0' - 10 : '0';
        $user_type  = $request->getQueryParams('user_type')['user_type'] ? $request->getQueryParams('user_type')['user_type'] : '1';
        $search     = $request->getQueryParams('search')['search'] ? $request->getQueryParams('search')['search'] : false;

        $users      = $this->userList->getUsers($page, $user_type, $search);

        $response->getBody()->write((string)json_encode($users));
        return $response->withHeader('Content-Type', 'application/json')->withStatus(200);
    }
}