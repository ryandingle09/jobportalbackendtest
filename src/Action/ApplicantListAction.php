<?php

namespace App\Action;

use App\Domain\Applicant\Service\ApplicantList;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class ApplicantListAction
{
    private $applicantList;

    public function __construct(ApplicantList $applicantList)
    {
        $this->applicantList = $applicantList;
    }

    public function __invoke(
        ServerRequestInterface $request, 
        ResponseInterface $response
    ): ResponseInterface {
        $page   = $request->getQueryParams('page')['page'] ? $request->getQueryParams('page')['page'].'0' - 10 : '0';
        $job_id = $request->getQueryParams('job_id')['job_id'] ? $request->getQueryParams('job_id')['job_id'] : false;
        $search = $request->getQueryParams('search')['search'] ? $request->getQueryParams('search')['search'] : false;

        $data   = $this->applicantList->getList($page, $job_id, $search);

        $response->getBody()->write((string)json_encode($data));

        return $response->withHeader('Content-Type', 'application/json')->withStatus(200);
    }
}