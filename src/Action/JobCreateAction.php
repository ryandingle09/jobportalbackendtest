<?php

namespace App\Action;

use App\Domain\Job\Service\JobCreator;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class JobCreateAction
{
    private $jobCreator;

    public function __construct(JobCreator $jobCreator)
    {
        $this->jobCreator = $jobCreator;
    }

    public function __invoke(
        ServerRequestInterface $request, 
        ResponseInterface $response
    ): ResponseInterface {
        $status_code    = 201;
        $status         = 'success';
        $message        = 'Successfully created!';
        $result         = [];

        $data       = (array)$request->getParsedBody();
        $id         = $this->jobCreator->create($data);
        $data       = $userId;
        $status     = 'success';
        $message    = 'Successfully Added!';

        $result = [
            'status'    => $status,
            'message'   => $message,
            'data'      => $id,
        ];

        $response->getBody()->write((string)json_encode($result));
        return $response->withHeader('Content-Type', 'application/json')->withStatus($status_code);
    }
}