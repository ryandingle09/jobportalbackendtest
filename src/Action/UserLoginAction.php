<?php

namespace App\Action;

use App\Domain\User\Service\UserCheck;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use \Firebase\JWT\JWT;

final class UserLoginAction
{
    private $UserCheck;

    public function __construct(UserCheck $UserCheck)
    {
        $this->UserCheck = $UserCheck;
    }

    public function __invoke(
        ServerRequestInterface $request, 
        ResponseInterface $response
    ): ResponseInterface {

        $status_code = 200;
        $data   = (array)$request->getParsedBody();

        $data = $this->UserCheck->get($data['email'], $data['password']);

        if($data)
        {
            $secret_key = "thisissamplesecretnigga";
            $issuer_claim = "www.ryandingle.dev";
            $audience_claim = "THE_AUDIENCE";
            $issuedat_claim = time();
            $notbefore_claim = $issuedat_claim + 10; 
            $expire_claim = $issuedat_claim + 1200;
            $token = [
                "iss" => $issuer_claim,
                "aud" => $audience_claim,
                "iat" => $issuedat_claim,
                "nbf" => $notbefore_claim,
                "exp" => $expire_claim,
                "data" => $data,
            ];

            $jwt = JWT::encode($token, $secret_key);

            $result = [
                'data' => $data, 
                'status' => 'success', 
                'message' => 'Successfully Logged In!',
                'token' => $jwt
            ];
        }
        else
        {
            $status_code = 422;
            $result      = ['status' => 'error' ,'message' => 'Invalid Credentials!'];
        }

        $response->getBody()->write((string)json_encode($result));
        return $response->withHeader('Content-Type', 'application/json')->withStatus($status_code);
    }
}