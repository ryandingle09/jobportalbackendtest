<?php

namespace App\Action;

use App\Domain\Applicant\Service\Get;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class ApplicantGetAction
{
    private $Get;

    public function __construct(Get $Get)
    {
        $this->Get = $Get;
    }

    public function __invoke(
        ServerRequestInterface $request, 
        ResponseInterface $response
    ): ResponseInterface {
        $id     = $request->getQueryParams('id') ? $request->getQueryParams('id')['id'] : false;
        $data   = $this->Get->get($id);

        $response->getBody()->write((string)json_encode($data));
        return $response->withHeader('Content-Type', 'application/json')->withStatus(200);
    }
}