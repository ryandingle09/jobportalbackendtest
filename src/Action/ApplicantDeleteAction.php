<?php

namespace App\Action;

use App\Domain\Applicant\Service\Delete;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class ApplicantDeleteAction
{
    private $Delete;

    public function __construct(Delete $Delete)
    {
        $this->Delete = $Delete;
    }

    public function __invoke(
        ServerRequestInterface $request, 
        ResponseInterface $response
    ): ResponseInterface {
        $id             = $request->getQueryParams('id')['id'] ? $request->getQueryParams('id')['id'] : false;
        $status_code    = 201;
        $status         = 'success';
        $message        = 'Successfully Deleted!';
        $result         = [];

        $data = $this->Delete->delete($id);

        if(!$data)
        {
            $data       = $id;
            $status     = 'error';
            $message    = 'Unexpected Error Encountered! Contact system administrator!';
        }

        $public_path    = dirname(__DIR__);
        $public_path    = str_replace('src','public/uploads', $public_path);
        $filename       = str_replace('/uploads/', '', $data);
        $filepath       = $public_path.'/'.$filename;

        if(file_exists($filepath))
            unlink($filepath);

        $result = [
            'status'    => $status,
            'message'   => $message,
            'data'      => $id,
        ];

        $response->getBody()->write((string)json_encode($result));
        return $response->withHeader('Content-Type', 'application/json')->withStatus($status_code);
    }
}