<?php

namespace App\Action;

use App\Domain\User\Service\UserUpdate;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class UserUpdateAction
{
    private $JobUpdate;

    public function __construct(UserUpdate $UserUpdate)
    {
        $this->UserUpdate = $UserUpdate;
    }

    public function __invoke(
        ServerRequestInterface $request, 
        ResponseInterface $response
    ): ResponseInterface {
        $status_code    = 201;
        $status         = 'success';
        $message        = 'Successfully udpated!';
        $result         = [];

        $data       = (array)$request->getParsedBody();
        $count      = 0;

        // check if not same email change
        if($data['email'] !== $data['email_old'])
        {
            $count = $this->UserUpdate->checkExists($data);

            if($count !== 0)
            {
                $status_code    = 422;
                $status         = 'error'; 
                $message        = 'Email address already exists!';
            }
            else
                $this->UserUpdate->update($data);
        }
        else
        {
            $this->UserUpdate->update($data);

            $status     = 'success';
            $message    = 'Successfully updated!';
        }

        $result = [
            'status'    => $status,
            'message'   => $message
        ];

        $response->getBody()->write((string)json_encode($result));
        return $response->withHeader('Content-Type', 'application/json')->withStatus($status_code);
    }
}