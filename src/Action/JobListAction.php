<?php

namespace App\Action;

use App\Domain\Job\Service\JobList;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class JobListAction
{
    private $jobList;

    public function __construct(JobList $jobList)
    {
        $this->jobList = $jobList;
    }

    public function __invoke(
        ServerRequestInterface $request, 
        ResponseInterface $response
    ): ResponseInterface {
        $page   = $request->getQueryParams('page')['page'] ? $request->getQueryParams('page')['page'].'0' - 10 : '0';
        $search = $request->getQueryParams('search')['search'] ? $request->getQueryParams('search')['search'] : false;

        $data = $this->jobList->getList($page, $search);

        $response->getBody()->write((string)json_encode($data));
        return $response->withHeader('Content-Type', 'application/json')->withStatus(200);
    }
}