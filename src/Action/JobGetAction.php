<?php

namespace App\Action;

use App\Domain\Job\Service\JobGet;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class JobGetAction
{
    private $jobGet;

    public function __construct(JobGet $jobGet)
    {
        $this->jobGet = $jobGet;
    }

    public function __invoke(
        ServerRequestInterface $request, 
        ResponseInterface $response
    ): ResponseInterface {
        $id     = $request->getQueryParams('id') ? $request->getQueryParams('id')['id'] : false;
        $data   = $this->jobGet->get($id);

        $response->getBody()->write((string)json_encode($data));
        return $response->withHeader('Content-Type', 'application/json')->withStatus(200);
    }
}