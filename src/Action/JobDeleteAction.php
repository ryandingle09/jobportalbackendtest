<?php

namespace App\Action;

use App\Domain\Job\Service\Delete;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class JobDeleteAction
{
    private $Delete;

    public function __construct(Delete $Delete)
    {
        $this->Delete = $Delete;
    }

    public function __invoke(
        ServerRequestInterface $request, 
        ResponseInterface $response
    ): ResponseInterface {
        $id             = $request->getQueryParams('id')['id'] ? $request->getQueryParams('id')['id'] : false;
        $status_code    = 201;
        $status         = 'success';
        $message        = 'Successfully Deleted!';
        $result         = [];

        $data = $this->Delete->delete($id);

        if(!$data)
        {
            $data       = $id;
            $status     = 'error';
            $message    = 'Unexpected Error Encountered! Contact system administrator!';
        }

        $result = [
            'status'    => $status,
            'message'   => $message,
            'data'      => $id,
        ];

        $response->getBody()->write((string)json_encode($result));
        return $response->withHeader('Content-Type', 'application/json')->withStatus($status_code);
    }
}