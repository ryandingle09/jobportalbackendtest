<?php

namespace App\Action;

use App\Domain\Job\Service\JobUpdate;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class JobUpdateAction
{
    private $JobUpdate;

    public function __construct(JobUpdate $JobUpdate)
    {
        $this->JobUpdate = $JobUpdate;
    }

    public function __invoke(
        ServerRequestInterface $request, 
        ResponseInterface $response
    ): ResponseInterface {
        $status_code    = 201;
        $status         = 'success';
        $message        = 'Successfully udpated!';
        $result         = [];

        $data       = (array)$request->getParsedBody();
        $id         = $this->JobUpdate->update($data);
        $data       = $userId;
        $status     = 'success';
        $message    = 'Successfully updated!';

        $result = [
            'status'    => $status,
            'message'   => $message
        ];

        $response->getBody()->write((string)json_encode($result));
        return $response->withHeader('Content-Type', 'application/json')->withStatus($status_code);
    }
}