<?php

namespace App\Action;

use App\Domain\Applicant\Service\ApplicantCreator;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class ApplicantCreateAction
{
    private $applicantCreator;

    public function __construct(ApplicantCreator $applicantCreator)
    {
        $this->applicantCreator = $applicantCreator;
    }

    public function __invoke(
        ServerRequestInterface $request, 
        ResponseInterface $response
    ): ResponseInterface {

        $result         = [];
        $status_code    = 201;
        $imagePath      = '';
        $files          = $request->getUploadedFiles();
        $public_path    = dirname(__DIR__);
        $public_path    = str_replace('src','public/uploads', $public_path);
        $file           = $files['resume'];

        if ($file->getError() == UPLOAD_ERR_OK ) {
            $filename = date('Y-m-d m:i:s').'-'.$file->getClientFilename();
            $filename = str_replace(' ', '-', $filename);
            $file->moveTo($public_path.'/'.$filename);
            $imagePath = '/uploads/'.$filename;

            $data   = (array)$request->getParsedBody();
            $data['resume'] = $imagePath;

            $id = $this->applicantCreator->create($data);

            $result = ['id' => $id, 'status' => 'success', 'message' => 'You have successfully apply to this job post!'];
        }
        else
        {
            $result      = ['status' => 'error' ,'message' => 'Unexpected Error Occured. Please contact software developer!'];
        }

        $response->getBody()->write((string)json_encode($result));
        return $response->withHeader('Content-Type', 'application/json')->withStatus($status_code);
    }
}