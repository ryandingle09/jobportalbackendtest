<?php

namespace App\Action;

use App\Domain\User\Service\UserGet;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class UserGetAction
{
    private $UserGet;

    public function __construct(UserGet $UserGet)
    {
        $this->UserGet = $UserGet;
    }

    public function __invoke(
        ServerRequestInterface $request, 
        ResponseInterface $response
    ): ResponseInterface {
        $id     = $request->getQueryParams('id') ? $request->getQueryParams('id')['id'] : false;
        $data   = $this->UserGet->get($id);

        $response->getBody()->write((string)json_encode($data));
        return $response->withHeader('Content-Type', 'application/json')->withStatus(200);
    }
}