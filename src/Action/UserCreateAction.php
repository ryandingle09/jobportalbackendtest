<?php

namespace App\Action;

use App\Domain\User\Service\UserCreator;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class UserCreateAction
{
    private $userCreator;

    public function __construct(UserCreator $userCreator)
    {
        $this->userCreator = $userCreator;
    }

    public function __invoke(
        ServerRequestInterface $request, 
        ResponseInterface $response
    ): ResponseInterface {
        $status_code    = 201;
        $status         = 'success';
        $message        = 'Successfully created!';
        $result         = [];

        $data   = (array)$request->getParsedBody();
        $check  = $this->userCreator->checkEmail($data['email']);
        
        if($check == '1')
        {
            $status_code    = 422;
            $status         = 'error';
            $message        = 'Email Address Already Exists!';
            $data           = '';
        }
        else
        {

            $userId = $this->userCreator->createUser($data);
            $data = $userId;
            $status = 'success';
            $message = 'Successfully Added!';
        }

        $result = [
            'status'    => $status,
            'message'   => $message,
            'data'      => $result,
        ];

        $response->getBody()->write((string)json_encode($result));
        return $response->withHeader('Content-Type', 'application/json')->withStatus($status_code);
    }
}