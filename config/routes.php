<?php

use Slim\App;

return function (App $app) {

    $app->get('/api/v1/', \App\Action\HomeAction::class);
    $app->options('/api/v1/', function (ServerRequestInterface $request, ResponseInterface $response): ResponseInterface {
        return $response;
    });
    // -----------------

    $app->post('/api/v1/user/add', \App\Action\UserCreateAction::class);
    $app->options('/api/v1/user/add', function (ServerRequestInterface $request, ResponseInterface $response): ResponseInterface {
        return $response;
    });
    
    $app->get('/api/v1/user/list', \App\Action\UserListAction::class);
    $app->options('/api/v1/user/list', function (ServerRequestInterface $request, ResponseInterface $response): ResponseInterface {
        return $response;
    });

    $app->get('/api/v1/user/get', \App\Action\UserGetAction::class);
    $app->options('/api/v1/user/get', function (ServerRequestInterface $request, ResponseInterface $response): ResponseInterface {
        return $response;
    });

    $app->get('/api/v1/user/delete', \App\Action\UserDeleteAction::class);
    $app->options('/api/v1/user/delete', function (ServerRequestInterface $request, ResponseInterface $response): ResponseInterface {
        return $response;
    });

    $app->post('/api/v1/user/update', \App\Action\UserUpdateAction::class);
    $app->options('/api/v1/user/update', function (ServerRequestInterface $request, ResponseInterface $response): ResponseInterface {
        return $response;
    });

    // -----------------


    $app->post('/api/v1/job/add', \App\Action\JobCreateAction::class);
    $app->options('/api/v1/job/add', function (ServerRequestInterface $request, ResponseInterface $response): ResponseInterface {
        return $response;
    });

    $app->post('/api/v1/job/update', \App\Action\JobUpdateAction::class);
    $app->options('/api/v1/job/update', function (ServerRequestInterface $request, ResponseInterface $response): ResponseInterface {
        return $response;
    });

    $app->get('/api/v1/job/list', \App\Action\JobListAction::class);
    $app->options('/api/v1/job/list', function (ServerRequestInterface $request, ResponseInterface $response): ResponseInterface {
        return $response;
    });

    $app->get('/api/v1/job/delete', \App\Action\JobDeleteAction::class);
    $app->options('/api/v1/job/delete', function (ServerRequestInterface $request, ResponseInterface $response): ResponseInterface {
        return $response;
    });

    $app->get('/api/v1/job/post', \App\Action\JobGetAction::class);
    $app->options('/api/v1/job/post', function (ServerRequestInterface $request, ResponseInterface $response): ResponseInterface {
        return $response;
    });
    
    // -----------------

    $app->get('/api/v1/applicant/list', \App\Action\ApplicantListAction::class);
    $app->options('/api/v1/applicant/list', function (ServerRequestInterface $request, ResponseInterface $response): ResponseInterface {
        return $response;
    });

    $app->get('/api/v1/applicant/get', \App\Action\ApplicantGetAction::class);
    $app->options('/api/v1/applicant/get', function (ServerRequestInterface $request, ResponseInterface $response): ResponseInterface {
        return $response;
    });

    $app->get('/api/v1/applicant/delete', \App\Action\ApplicantDeleteAction::class);
    $app->options('/api/v1/applicant/delete', function (ServerRequestInterface $request, ResponseInterface $response): ResponseInterface {
        return $response;
    });

    $app->post('/api/v1/applicant/apply', \App\Action\ApplicantCreateAction::class);
    $app->options('/api/v1/applicant/apply', function (ServerRequestInterface $request, ResponseInterface $response): ResponseInterface {
        return $response;
    });

    $app->post('/api/v1/auth/login', \App\Action\UserLoginAction::class);
    $app->options('/api/v1/auth/login', function (ServerRequestInterface $request, ResponseInterface $response): ResponseInterface {
        return $response;
    });
};