<?php

use Selective\BasePath\BasePathMiddleware;
use Selective\Config\Configuration;
use Slim\App;
use Slim\Middleware\ErrorMiddleware;


return function (App $app) {
    // Parse json, form data and xml
    $app->addBodyParsingMiddleware();

    // $app->add(\App\Middleware\JwtCheck::class);

    $app->add(\App\Middleware\CorsMiddleware::class);

    // Add the Slim built-in routing middleware
    $app->addRoutingMiddleware();
    $app->addErrorMiddleware(true, true, true);

    $app->add(BasePathMiddleware::class);

    // Catch exceptions and errors
    $app->add(ErrorMiddleware::class);
};