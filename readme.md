# REQUIREMENTS
 - Php Vesion 7.4
 - Nodejs V10.16.0 
 - Nginx Web Server

# TECHNOLOGY
 - Php Slim Framework Version 4
 - Angular TypeScript Version 8

# INTRUCTIONS
`Please use nginx for local webserver`
`localhost domain "jobportal.test" for the backend php api to avoid changes in the frontend service  url code app`

# Install dependencies
`for frontend make sure to follow the requirements and install the angular/cli version 8 and type`
 - npm install 
`to run type`
ng server
`open browser localhost:4200`

`for frontend make sure to follow the requirements and use nginx with localhostname 'jobportal.test' and follow the config below`
 - composer install 
`open browser jobportal.test/api/v1/`

# NGINX Config
`
server {
    listen 80;
    server_name jobportal.test;
    index index.php;
    error_log /home/user/logs/jobportal.error.log;
    access_log /home/user/jobportal.access.log;
    root /home/user/workspace/jobportalbackend/public;

    location / {
        try_files $uri /index.php$is_args$args;
    }

    location ~ \.php {
        try_files $uri =404;
        fastcgi_split_path_info ^(.+\.php)(/.+)$;
        include fastcgi_params;
        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
        fastcgi_param SCRIPT_NAME $fastcgi_script_name;
        fastcgi_index index.php;
        fastcgi_pass unix:/var/run/php/php7.4-fpm.sock;
    }
}
`